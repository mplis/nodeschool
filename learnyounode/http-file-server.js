var http = require('http'),
    fs = require('fs')

var server = http.createServer(function (request, response) {
    file = fs.createReadStream(process.argv[3])
    file.pipe(response)
})

server.listen(Number(process.argv[2]))