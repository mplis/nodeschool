var net = require('net'),
    strftime = require('strftime')

var myServer = net.createServer(function (socket) {
    socket.write(strftime('%F %R\n'))
    socket.end()
})

myServer.listen(Number(process.argv[2]))