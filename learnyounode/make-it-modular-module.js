var fs = require('fs'),
    path = require('path');

module.exports = function (dirname, ext, callback) {
    fs.readdir(dirname, function(err, list) {
        if (err) {
            callback(err)
        } else {
            var files = list.filter(function (file) {
                return path.extname(file) === '.' + ext
            })
            callback(null, files)
        }
    })
}