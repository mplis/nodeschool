var http = require('http')
    bl = require('bl'),
    results = [],
    count = 0

function printResults() {
    results.forEach(function(result) {
        console.log(result)
    })
}

function myHttp (index) {
    http.get(process.argv[2 + index], function(response) {
        response.pipe(bl(function (err, data) {
            if (err)
                return console.error(err)
            dataStr = data.toString()
            
            results[index] = dataStr
            count++

            if (count == 3) {
                printResults()
            }
        }))
    })
}

for (var i = 0; i < 3; i++) {
    myHttp(i)
}