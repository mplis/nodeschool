var mymod = require('./make-it-modular-module')

mymod(process.argv[2], process.argv[3], function(err, files) {
    if (err) {
        return console.log('There was an error: ' + err)
    }
    files.forEach(function (file) {
        console.log(file)
    });
});