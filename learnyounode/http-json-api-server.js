var http = require('http'),
    url  = require('url')

var server = http.createServer(function (request, response) {
    if (request.method != 'GET')
        return response.end('Send me a GET')

    var parsedUrl = url.parse(request.url, true)

    if (parsedUrl.pathname === '/api/parsetime') {
        response.writeHead(200, { 'Content-Type': 'application/json' })
        var date = new Date(parsedUrl.query['iso'])

        var obj = {
            'hour': date.getHours(),
            'minute': date.getMinutes(),
            'second': date.getSeconds()
        }
        response.end(JSON.stringify(obj))
    // } else if (parsedUrl.pathName === '/api/unixtime') {
    } else {
        response.writeHead(200, { 'Content-Type': 'application/json' })
        var obj = {
            "unixtime": Date.parse(parsedUrl.query['iso'])
        }
        response.end(JSON.stringify(obj))
    }
})

server.listen(process.argv[2])