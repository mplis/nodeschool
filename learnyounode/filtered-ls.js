var fs = require('fs'),
    path = require('path')
    dirname = process.argv[2],
    ext = process.argv[3]

fs.readdir(dirname, function (err, list) {
    for (var i = 0; i < list.length; i++) {
        f = list[i]
        if (path.extname(f) === '.' + ext) {
            console.log(f)
        }
    }
})