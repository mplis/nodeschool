var http = require('http')
    bl = require('bl')

function myHttp (index) {
    http.get(process.argv[2 + index], function(response) {
        response.pipe(bl(function (err, data) {
            if (err)
                return console.error(err)
            dataStr = data.toString()
            console.log(dataStr)
        }))
    })
}

for (var i = 0; i < 3; i++) {
    myHttp(i)
}