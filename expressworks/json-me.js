var express = require('express')
var fs      = require('fs')

var app = express()

app.get('/books', function(req, res) {
    var filename = process.argv[3]
    fs.readFile(filename, function(err, data) {
        res.json(JSON.parse(data))
    })
})

app.listen(process.argv[2])