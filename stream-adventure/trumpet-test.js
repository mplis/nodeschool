var trumpet = require('trumpet');
var through = require('through2');
var tr = trumpet();

var louds = tr.selectAll('.loud', function(loud) {
    var str = loud.createStream()
    str.pipe(through(function (buf, _, next) {
        this.push(buf.toString().toUpperCase());
        next();
    })).pipe(str);
});

// var loud = tr.select('.loud').createStream();
// loud.pipe(through(function (buf, _, next) {
//     this.push(buf.toString().toUpperCase());
//     next();
// })).pipe(loud);

// var quiet = tr.select('.quiet').createStream();
// quiet.pipe(through(function (buf, _, next) {
//     this.push(buf.toString().toLowerCase());
//     next();
// })).pipe(quiet);

process.stdin.pipe(tr).pipe(process.stdout);