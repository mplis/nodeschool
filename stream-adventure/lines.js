var through = require('through2'),
    split   = require('split')

var toUpper = false

process.stdin
    .pipe(split())
    .pipe(through(function (line, _, next) {
        if (toUpper)
            this.push(line.toString().toUpperCase() + '\n');
        else
            this.push(line.toString().toLowerCase() + '\n');
        toUpper = !toUpper
        next();
    }))
    .pipe(process.stdout)
;