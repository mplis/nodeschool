var http    = require('http'),
    through = require('through2');

function write (buf, _, next) {
    this.push(buf.toString().toUpperCase());
    next();
}

var server = http.createServer(function(req, res) {
    if (req.method === 'POST') {
        req.pipe(through(write)).pipe(res);
    }
    else res.end('send me a POST\n');
})

server.listen(process.argv[2])